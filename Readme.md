# Pull Request Driven Development
## Outline
The repository is for learning pull request driven development.

## What's pull request driven?
With creating pull request before start coding, Issues and commits would be associated. After created pull request, you can track all commits and conversation about the branch in a pull request. It makes review be easy and makes your code get better.

## Flow

1\. Create new branch for related issue.
```
git checkout -b "#[your redmine issue number]"
git push origin "#[your redmine issue number]"
```

https://bitbucket.org/mulodo_Tran-Tuan/sample_pull_request_driven/src/560fe9f454b27f03cb5eef82ba4f494226937cde?at=#1_IssueName

2\. To do empty commit for creating new pull request.
```
git commit --allow-empty
git push origin "#[your redmine issue number]"
```
https://bitbucket.org/mulodo_Tran-Tuan/sample_pull_request_driven/commits/branch/%231_IssueName

3\. To create new pull request.

- To add prefix "[WIP]" and suffix "#[your issue number]" to the tile.
- "[WIP]" means "Work in progress". It indicates the issue work in progress.
- At pull request description. For making your task be clear, write issue as TODO list with the following format. 

```
e.g

## Outline
Writing outline of the issue at here.
{full_path_to_target_class_file}

## TestCase for class classsTargetName
- target function: functionTargetName - {Coverage level}
- - testFunctionName
- - testFunctionName
- target function: functionTargetName - {Coverage level}
- - testFunctionName
- - testFunctionName

## Additional Tasks
If new tasks are added after start working,
write them at here for understanding those tasks are additional tasks.
- foo Bar
- foo Bar
- foo Bar

## Related issues
If a issue has related tasks, write link as issue number at here.

## Parent issue
If a issue has parent issue, write link as issue number at here.

#### Note
If a issue has some notes, write it at here.
```

https://bitbucket.org/mulodo_Tran-Tuan/sample_pull_request_driven/pull-requests/1/wip-1_issuename/diff

4\. Start coding.

https://bitbucket.org/mulodo_Tran-Tuan/sample_pull_request_driven/pull-requests/1/wip-1_issuename/activity

5\. Request review code.

Remove the prefix "[WIP]" of the title, add a comment on your pull request, tag the reviewer's name with messagge

https://bitbucket.org/mulodo_Tran-Tuan/sample_pull_request_driven/commits/560fe9f454b27f03cb5eef82ba4f494226937cde?at=master

6\. Request merge branch.

After finish reviewing and get confirmed, you can add prefix "[Waiting Merge]" to the title